from osdoc2.Metadata import ApplicationAlternateName, ApplicationDescription, ApplicationId, ApplicationName, ApplicationOwner, SheetMetadataDict
from osdoc2.SheetMetadata import SheetMetadata
from osdoc2.Template import appDetails, page

from osdoc2.Template import renderModelList, renderModelHeader, renderCellList, renderRulesView

def generateHomePage():
    docfile = file("docs\\index.htm", "w")
    docfile.write(page % (ApplicationName, ApplicationDescription, ApplicationName, renderModelList(SheetMetadataDict), ""))
    docfile.close()
    

def generateApplicationDetails():
    pagecontent = appDetails % (ApplicationId, ApplicationName, ApplicationAlternateName, ApplicationDescription, ApplicationOwner)
    docfile = file("docs\\application.htm", "w")
    docfile.write(page % (ApplicationName, ApplicationDescription, ApplicationName, renderModelList(SheetMetadataDict), pagecontent))
    docfile.close()
    
def generateModelDetails():
    for key in SheetMetadataDict:
        try:
            print 'processing %s...' % (SheetMetadataDict[key])
            SheetMetadataId = key
            sheetmetadata = SheetMetadata(SheetMetadataId)
            modelheader = renderModelHeader(sheetmetadata.getSheetName(), SheetMetadataId, sheetmetadata.showInWorktop(), sheetmetadata.IsPublic(), sheetmetadata.IsHidden())
            cellinformation = renderCellList(sheetmetadata.getAllCellMetadata())
            rulesview = renderRulesView(sheetmetadata)
            pagecontent = '%s%s%s' % (modelheader, cellinformation, rulesview)
            docfile = file("docs\\" + SheetMetadataId + ".htm", "w")
            docfile.write(page % (ApplicationName, ApplicationDescription, ApplicationName, renderModelList(SheetMetadataDict), pagecontent))
            docfile.close()
        except:
            import sys
            print "Unexpected error:", sys.exc_info()[0]
            
def init():
    generateHomePage()
    generateApplicationDetails()
    generateModelDetails()

init()