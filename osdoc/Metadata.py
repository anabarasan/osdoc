#metadata.py
from orangescape.application.Application import Application

def getApplicationId():
    return Application["root"]

def getApplicationName():
    try:
        return Application[getApplicationId()]["Application_Name"]
    except:
        return ''

def getApplicationAltName():
    return Application[getApplicationId()]["SheetName"]

def getApplicationOwner():
    return Application[getApplicationId()]["ApplicationOwner"]

def getApplicationDescription():
    try:
        return Application[getApplicationId()]["ApplicationDescription"]
    except:
        return ''

def getAppRolesDict():
    return Application[getApplicationId()]["AppRoles"]

def getApplicationObject():
    return Application[getApplicationId()]

def getSheetMetadataDict():
    return Application[getApplicationId()]["Application.SheetMetadata"]

def getFolderDict():
    return Application[getApplicationId()]["Application.Folder"]