# Rule Design Perspective related.
def getRows(sheetmetadata): # RETURNS a dict with rowno as key and rowid as value
    #get row data
    rows = sheetmetadata[sheetmetadata["root"]]["SheetMetadata.RowMetadata"]

    #get RowNumber
    temp = {}
    for key in rows:
            row = sheetmetadata[key]
            temp[row["RowNumber"]] = key

    return temp

def getCells(sheetmetadata, rowmetadata): # RETURNS a dict with cellname as key and cell id as value
    #get Cells of Row
    RowCellMetadata = sheetmetadata[rowmetadata]["RowMetadata.CellMetadata"]
    temp = {}
    #get cellname and reverse
    for key in RowCellMetadata:
        try:
            temp[sheetmetadata[key]["CellDisplayName"]]=key
        except:
            temp[RowCellMetadata[key]]=key
    return temp

def viewRules(sheetmetadata):
    SheetName = sheetmetadata[sheetmetadata['root']]['SheetName']
    rules = file("docs\\rules\\" + sheetmetadata['root'] + ".htm", "w")
    rules.write("<!doctype html><html><head><title>%s</title>" % (SheetName))
    rules.write("<style>td{ width:100px; height:22px; font-size:small; border:1px dotted;} .header{background-color:rgb(228, 236, 247); text-align:center; font-weight:bold;} .public{background-color:rgb(209, 232, 255);} .reference{background-color:rgb(255, 237, 219);}")
    rules.write("#CellNameFormulaBar .column{position:relative; float:left; border:1px dotted; height:22px;} #CellName{width:100px; border:1px dotted;} #label{text-align:right; width:100px;} #formulaBar{width:1000px; border:1px dotted;}</style>")
    rules.write("</head><body><h2 style='text-align:center;'>%s</h2>" % (SheetName))
    rules.write(rulesHeader())
    rules.write("<table style='border:1px solid;' width=100%>")
    rowdata = getRows(sheetmetadata)
    rowName = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ']
    for key in sorted(rowdata.iterkeys()):
        if key > 0:
            celldata = getCells(sheetmetadata,rowdata[key])
            if key == 1:
                rules.write("<tr id='rowHeader'><td class='header'>&nbsp;</td>")
                count=0
                while count < len(celldata):
                    rules.write("<td class='header'>%s</td>" % (rowName[count]))
                    count = count+1
                rules.write("</tr>")
            rules.write("<tr id='%s'><td class='header'>%s</td>" % (rowdata[key],sheetmetadata[rowdata[key]]['RowNumber']))
            for key in sorted(celldata.iterkeys()):
                try:
                    try:
                        if sheetmetadata[celldata[key]]["ReferredSheetMetadata"]:
                            style = 'reference'
                    except:
                        style = 'public'
                    rules.write("<td id = '%s' class='%s'><strong>%s</strong></td>" % (celldata[key], style, sheetmetadata[celldata[key]]["CellName"]))
                except:
                    #rules.write("<td id= '%s'>%s</td>" % (celldata[key],key))
                    rules.write("<td id= '%s'>&nbsp;</td>" % (celldata[key]))
            rules.write("</tr>")
    rules.write("</table></body></html>")
    rules.close()

def rulesHeader():
    header = "<div id='CellNameFormulaBar'><div id='CellName' class='column' >&nbsp;</div><div id='label' class='header column'><i>f</i>(x)</div><div id='formulaBar' class='column'>&nbsp;</div></div>"
    return header