#Template.py

#NamedCellDetails = "<table><tr><td style='border-bottom:1px solid;'> Id : </td><td colspan='3' style='border-bottom:1px solid;'> {SheetId} </td></tr><tr><td style='border-bottom:1px solid;'> Format : </td><td style='border-bottom:1px solid;'> {FinalFormat} </td><td style='border-left:1px solid; border-bottom:1px solid;'> Address : </td><td style='border-bottom:1px solid;'> {CellDisplayName} </td></tr><tr><td>Reference Cell :</td><td>{ReferenceCell}</td><td style='border-left:1px solid;'>Formula Cell :</td><td>{FormulaCell}</td></tr></table>"
NamedCellDetails = "<td> {SheetId} </td><td> {FinalFormat} </td><td style='text-align:center;'> {CellDisplayName} </td><td style='text-align:center;'>{ReferenceCell}</td><td style='text-align:center;'>{FormulaCell}</td>"
NameCellTemplate = "<tr style='background-color:#d9fdfd' onmouseover='javascript:fieldover(this);' onmouseout='javascript:fieldout(this);'><td style='vertical-align:top;'> {CellName} </td>%s</tr>" % (NamedCellDetails)

def renderHeader(appName, appDescription):
    return "<h2 style='text-align:center;'> %s </h2><h3 style='text-align:center;'> %s </h3>" % (appName, appDescription)
    
def renderModelList(SheetMetadataDict):
    modellist = ""
    for key, value in sorted(SheetMetadataDict.iteritems(), key=lambda (k,v): (v,k)):
        modellist = modellist + "\t<option value='" + key + ".htm'>" + value + "</option>\n"
    return modellist
    
def renderModelHeader(modelName, modelId, showInWorktop, isPublic, isHidden):
    content = "<div style='text-align:center; font-weight:bold;'> %s </div>" % (modelName)
    content += "<table style='width:700px;'>"
    content += "<tr class='odd'><td>Id </td><td> %s </td></tr>" % (modelId)
    content += "<tr class='even'><td>Show In Worktop </td><td> %s </td></tr>" % (showInWorktop)
    content += "<tr class='odd'><td>Is Public </td><td> %s </td></tr>" % (isPublic)
    content += "<tr class='even'><td>Hide From Menu </td><td> %s </td></tr>" % (isHidden)
    content += "</table><hr/>"
    return content

def renderCellList(cellmetadatadict):
    
    PublicCells = "<table id='tblNamedCells' style='width:1000px;'>"
    PublicCells += "<tr><td colspan='6'><table align='center'><tr><td style='text-align:center;'> <b>Named Cells</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td><td style='text-align:center;'> <a href='javascript:void();' onClick='javascript:privateCells();'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Private Cells&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a> </td><td style='text-align:center;'> <a href='javascript:void();' onClick='javascript:systemCells();'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Cells </a> </td></tr></table></td></tr>"
    PublicCells += "<tr><th>Name</th><th>CellMetadataId</th><th>Format</th><th>CellAddress</th><th>ReferenceCell</th><th>FormulaCell</th></tr>"

    PrivateCells = "<table id='tblPrivateCells' style='width:1000px; display:none;'>"
    PrivateCells += "<tr><td colspan='6'><table align='center'><tr><td style='text-align:center;'>  <a href='javascript:void();' onClick='javascript:namedCells();'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Named Cells&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a> </td><td style='text-align:center;'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Private Cells</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td><td style='text-align:center;'> <a href='javascript:void();' onClick='javascript:systemCells();'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Cells </a> </td></tr></table></td></tr>"
    PrivateCells += "<tr><th>Name</th><th>CellMetadataId</th><th>Format</th><th>CellAddress</th><th>ReferenceCell</th><th>FormulaCell</th></tr>"
    
    SystemCells = "<table id='tblSystemCells' style='width:1000px; display:none;'>"
    SystemCells += "<tr style='border-bottom:1px solid;'><td colspan='6'><table align='center'><tr><td style='text-align:center;'>  <a href='javascript:void();' onClick='javascript:namedCells();'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Named Cells&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a> </td><td style='text-align:center;'> <a href='javascript:void();' onClick='javascript:privateCells();'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Private Cells&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a> </td><td style='text-align:center;'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>System Cells</b> </td></tr></table></td></tr>"
    SystemCells += "<tr><th>Name</th><th>CellMetadataId</th><th>Format</th><th>CellAddress</th><th>ReferenceCell</th><th>FormulaCell</th></tr>"
    
    for key in cellmetadatadict:
        cellmetadata = cellmetadatadict[key]
        try:
            if cellmetadata["ReferredSheetMetadata"]:
                ReferenceCell = 'Yes'
        except:
            ReferenceCell = 'No'
        try:
            if cellmetadata["isFormulaCell"]:
                FormulaCell = 'Yes'
        except:
            FormulaCell = 'No'
            
        if cellmetadata["RowNumber"] > 0 :
            if cellmetadata.get("SheetMetadata") is not None:
                try:
                    PublicCells += NameCellTemplate.replace('{CellName}', cellmetadata["CellName"]).replace('{SheetId}', cellmetadata["SheetId"]).replace('{FinalFormat}', cellmetadata["FinalFormat"]).replace('{CellDisplayName}', cellmetadata["CellDisplayName"]).replace('{ReferenceCell}', ReferenceCell).replace('{FormulaCell}', FormulaCell)
                except:
                    PublicCells += NameCellTemplate.replace('{CellName}', cellmetadata["CellName"]).replace('{SheetId}', cellmetadata["SheetId"]).replace('{CellDisplayName}', cellmetadata["CellDisplayName"]).replace('{ReferenceCell}', ReferenceCell).replace('{FormulaCell}', FormulaCell)
            else :
                try:
                    PrivateCells += NameCellTemplate.replace('{CellName}', '').replace('{SheetId}', cellmetadata["SheetId"]).replace('{FinalFormat}', cellmetadata["FinalFormat"]).replace('{CellDisplayName}', cellmetadata["CellDisplayName"]).replace('{ReferenceCell}', ReferenceCell).replace('{FormulaCell}', FormulaCell)
                except:
                    PrivateCells += NameCellTemplate.replace('{CellName}', '').replace('{SheetId}', cellmetadata["SheetId"]).replace('{CellDisplayName}', cellmetadata["CellDisplayName"]).replace('{ReferenceCell}', ReferenceCell).replace('{FormulaCell}', FormulaCell)
        else :
            SystemCells += NameCellTemplate.replace('{CellName}', cellmetadata["CellName"]).replace('{SheetId}', cellmetadata["SheetId"]).replace('{FinalFormat}', cellmetadata["FinalFormat"]).replace('{CellDisplayName}', cellmetadata["CellDisplayName"]).replace('{ReferenceCell}', ReferenceCell).replace('{FormulaCell}', FormulaCell)
    
    PublicCells  += "</table>"
    PrivateCells += "</table>"
    SystemCells  += "</table>"
    
    return "<div style='float:left; width:100%;overflow-y:auto;'>" + PublicCells + PrivateCells + SystemCells + "</div>"
