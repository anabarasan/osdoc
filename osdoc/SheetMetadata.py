#SheetMetadata.py

def getSheetMetadata(SheetId):
    exec 'from orangescape.application.%s import %s as Model'% (SheetId, SheetId)
    return Model
    #__import__('orangescape.application.%s' % (SheetId))

def default():
    return ''

def getSheetName(SheetId):
    try:
        return getSheetMetadata(SheetId)[SheetId]["SheetName"]
    except:
        default()

def IsPublic(SheetId):
    try:
        if getSheetMetadata(SheetId)[SheetId]["IsPublic"]:
            return 'True'
    except:
        return 'False'
    
def showInWorktop(SheetId):
    try:
        if IsPublic(SheetId):
            if getSheetMetadata(SheetId)[SheetId]["DisableProcessInstance"]:
                return 'False'
    except:
        return 'True'
    
def IsHidden(SheetId):
    try:
        if IsPublic(SheetId):
            if getSheetMetadata(SheetId)[SheetId]["IsHidden"]:
                return 'True'
    except:
        return 'False'
    
def getCellMetadataDict(SheetId):
    try:
        return getSheetMetadata(SheetId)[SheetId]["SheetMetadata.CellMetadata"]
    except:
        default()

def getCellMetadata(SheetId, CellId):
    try:
        return getSheetMetadata(SheetId)[CellId]
    except:
        default()
        
def getAllCellMetadata(SheetId):
    try:
        cellDict = dict()
        sheetDict = getSheetMetadata(SheetId)
        for key in sheetDict:
            if key.startswith("Ce"):
                cellDict[key] = sheetDict[key]
        return cellDict
    except:
        default()