#main.py
from osdoc import Metadata, Template, SheetMetadata, Rules

appId             = Metadata.getApplicationId()
appName           = Metadata.getApplicationName()
if (appName == 'CLEditor'):
    appName       = Metadata.getApplicationAltName()
altName           = Metadata.getApplicationAltName()
appOwner          = Metadata.getApplicationOwner()
appDescription    = Metadata.getApplicationDescription()
SheetMetadataDict = Metadata.getSheetMetadataDict()

def basecontent(content):
    default = "<!doctype html><html style='height:100%;'><head>"
    default += "<link rel='stylesheet' type='text/css' href='styles.css'><script src='script.js' language='javascript'></script></head>"
    default += "<body style='height:100%;'><div>"
    default += "<header>" + Template.renderHeader(appName, appDescription) + "</header>"
    default += "<div style='text-align:right;'><select id='nav' style='width:auto; align:right;'  onChange='loadModel()'>\n"
    default += "<option value='application.htm'>" + appName + "</option>"
    default += Template.renderModelList(SheetMetadataDict)
    default += "</select> &nbsp;&nbsp;&nbsp; <a id='navlink' href='application.htm'> Go </a></div>"
    default += "<div id='content' style='float:left; width:100%;' align = center>"
    default += content
    default += "</div></div></body></html>"
    return default

def homePage():
    docfile = file("docs\\index.htm", "w")
    docfile.write(basecontent(''))
    docfile.close()
    
    rulefile = file("docs\\rules\\index.htm", "w")
    rulefile.write(basecontent(''))
    rulefile.close()
    
def application():
    content = "<table style='width:700px;'>"
    content += "<tr class='odd'><td>Id </td><td>" + appId + "</td></tr>"
    content += "<tr class='even'><td>Name </td><td>" + appName + "</td></tr>"
    content += "<tr class='odd'><td>Name (alt) </td><td>" + altName + "</td></tr>"
    content += "<tr class='even'><td>Description </td><td>" + appDescription + "</td></tr>"
    content += "<tr class='odd'><td>Owner </td><td>" + appOwner + "</td></tr>"
    content += "</table>"
    
    docfile = file("docs\\application.htm", "w")
    docfile.write(basecontent(content))
    docfile.close()
    
def model():

    for key in SheetMetadataDict:
        try:
            print 'processing %s...' % (SheetMetadataDict[key])
            SheetMetadataId = key
            
            Rules.viewRules(SheetMetadata.getSheetMetadata(SheetMetadataId)) #Prepare Rules Perspective View
            
            content = Template.renderModelHeader(SheetMetadata.getSheetName(SheetMetadataId), SheetMetadataId, SheetMetadata.showInWorktop(SheetMetadataId), SheetMetadata.IsPublic(SheetMetadataId), SheetMetadata.IsHidden(SheetMetadataId))
            content += Template.renderCellList(SheetMetadata.getAllCellMetadata(SheetMetadataId))
    
            docfile = file("docs\\" + SheetMetadataId + ".htm", "w")
            docfile.write(basecontent(content))
            docfile.close()
        except:
            import sys
            print "Unexpected error:", sys.exc_info()[0]

def init():
    homePage()
    application()
    model()
    
init()