function fieldover(control){
    control.style.backgroundColor='#ff5000'
    //control.style.fontWeight='bold'
}

function fieldout(control){
    control.style.backgroundColor='#d9fdfd'
    control.style.fontWeight='normal'
}

function systemCells(){
	document.getElementById('tblSystemCells').style.display = ''
	document.getElementById('tblNamedCells').style.display = 'none'
	document.getElementById('tblPrivateCells').style.display = 'none'
}

function namedCells(){
	document.getElementById('tblNamedCells').style.display = ''
	document.getElementById('tblSystemCells').style.display = 'none'
	document.getElementById('tblPrivateCells').style.display = 'none'
}

function privateCells(){
	document.getElementById('tblPrivateCells').style.display = ''
	document.getElementById('tblSystemCells').style.display = 'none'
	document.getElementById('tblNamedCells').style.display = 'none'
}

function loadModel(){
	document.getElementById('navlink').href = document.getElementById('nav').value;
}

var getElement = function(e){
	return document.getElementById(e)
}

var toggleCellInfo = function(celltype){
	elements = ['tblNamedCells', 'tblPrivateCells', 'tblSystemCells', 'tblRulesView', 'lblNamedCells', 'lblPrivateCells', 'lblSystemCells', 'lblRulesView', 'lnkNamedCells', 'lnkPrivateCells', 'lnkSystemCells', 'lnkRulesView']
	for (var i = 0; i < elements.length; i++){
		element = elements[i]
		if (((element.indexOf(celltype) > -1) && ((element.indexOf('tbl') > -1) || (element.indexOf('lbl') > -1))) || ((element.indexOf(celltype) == -1) && (element.indexOf('lnk') > -1))) {
			console.info(element + ' visible')
			getElement(element).style.display = ''
		} else {
			console.info(element + ' invisible')
			getElement(element).style.display = 'none'
		}
	}
    console.log('conmplete')
}

var init = function(){
	toggleCellInfo('NamedCells')
}
