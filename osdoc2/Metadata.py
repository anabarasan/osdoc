# metadata.py

from orangescape.application.Application import Application

def getApplicationName():
    try:
        apname = Application["Application_Name"]
        if (apname == 'CLEditor'):
            apname = Application["SheetName"]
        return apname
    except:
        return Application["SheetName"]  # Application Alternate Name
    
def getApplicationDescription():
    try:
        return Application["ApplicationDescription"]
    except:
        return ''
    
ApplicationId             = Application["root"]
Application               = Application[ApplicationId]
ApplicationName           = getApplicationName()
ApplicationAlternateName  = Application["SheetName"]
ApplicationOwner          = Application["ApplicationOwner"]
ApplicationDescription    = getApplicationDescription()
#AppRolesDict              = Application["AppRoles"]
FolderDict                = Application["Application.Folder"]
SheetMetadataDict         = Application["Application.SheetMetadata"]