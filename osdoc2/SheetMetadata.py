# SheetMetadata.py

class SheetMetadata:
    def __init__(self, SheetId):
        exec 'from orangescape.application.%s import %s as Model'% (SheetId, SheetId)
        self.id = SheetId
        self.SheetMetadata = Model
        self.model = Model[SheetId]

    def getSheetName(self):
        try:
            return self.model["SheetName"]
        except:
            return ''
    
    def IsPublic(self):
        try:
            if self.model["IsPublic"]:
                return 'True'
        except:
            return 'False'
        
    def showInWorktop(self):
        try:
            if self.IsPublic():
                if self.model["DisableProcessInstance"]:
                    return 'False'
        except:
            return 'True'
        
    def IsHidden(self):
        try:
            if self.IsPublic():
                if self.model["IsHidden"]:
                    return 'True'
        except:
            return 'False'
        
    def getCellMetadataDict(self):
        try:
            return self.model["SheetMetadata.CellMetadata"]
        except:
            return None
    
    def getCellMetadata(self, CellId):
        try:
            return self.SheetMetadata[CellId]
        except:
            return None
            
    def getAllCellMetadata(self):
        try:
            cellDict = dict()
            sheetDict = self.SheetMetadata
            for key in sheetDict:
                if key.startswith("Ce"):
                    cellDict[key] = sheetDict[key]
            return cellDict
        except:
            return None
        
    def getRows(self):
        #get row data
        rows = self.model["SheetMetadata.RowMetadata"]
        #get RowNumber
        rowdict = {}
        for key in rows:
                row = self.SheetMetadata[key]
                rowdict[row["RowNumber"]] = key
    
        return rowdict
    
    def getCells(self, RowMetadataId):
        #get Cells of Row
        RowCellMetadata = self.SheetMetadata[RowMetadataId]["RowMetadata.CellMetadata"]
        celldict = {}
        #get cellname and reverse
        for key in RowCellMetadata:
            try:
                celldict[self.SheetMetadata[key]["CellDisplayName"]]=key
            except:
                celldict[RowCellMetadata[key]]=key
        return celldict