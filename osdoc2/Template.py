page = """
<!doctype html>
    <html style='height:100%%;'>
        <head>
            <link rel='stylesheet' type='text/css' href='styles.css'>
            <script src='script.js' language='javascript'></script>
        </head>
    <body style='height:100%%;'>
        <div>
            <header>
                <h2 style='text-align:center;'> %s </h2>
                <h3 style='text-align:center;'> %s </h3>
            </header>
            <div style='text-align:right;'>
                <select id='nav' style='width:auto; align:right;'  onChange='loadModel()'>
                    <option value='application.htm'>%s</option>
                    %s
                </select>
                &nbsp;&nbsp;&nbsp; 
                <a id='navlink' href='application.htm'> Go </a>
            </div>
            <div id='content' style='float:left; width:100%%;' align = center> 
                %s
            </div>
            <div id='footer' style='height:15px; width:100%%;'>&nbsp;</div>
        </body>
        <script>
            init()
        </script>
    </html>
""" # % (appName, appDescription, appName, Template.renderModelList(SheetMetadataDict), content)
    
appDetails = """
    <table style='width:700px;'>
        <tr class='odd'><td>Id </td><td>%s</td></tr>
        <tr class='even'><td>Name </td><td>%s</td></tr>
        <tr class='odd'><td>Name (alt) </td><td>%s</td></tr>
        <tr class='even'><td>Description </td><td>%s</td></tr>
        <tr class='odd'><td>Owner </td><td>%s</td></tr>
    </table>
""" # % (appId, appName, altName, appDescription, appOwner)
    
NameCellTemplate = """
<tr style='background-color:#d9fdfd' onmouseover='javascript:fieldover(this);' onmouseout='javascript:fieldout(this);'>
    <td style='vertical-align:top;'> %s </td>
    <td> %s </td>
    <td> %s </td>
    <td style='text-align:center;'> %s </td>
    <td style='text-align:center;'> %s </td>
    <td style='text-align:center;'> %s </td>
</tr>\n
""" # % (CellName, SheetId, FinalFormat, CellDisplayName, ReferenceCell, FormulaCell)

ModelHeader = """
    <div style='text-align:center; font-weight:bold;'> %s </div>
    <table style='width:700px;'>
        <tr class='odd'><td>Id </td><td> %s </td></tr>
        <tr class='even'><td>Show In Worktop </td><td> %s </td></tr>
        <tr class='odd'><td>Is Public </td><td> %s </td></tr>
        <tr class='even'><td>Hide From Menu </td><td> %s </td></tr>
    </table>
    <hr/>
""" # % (modelName, modelId, showInWorktop, isPublic, isHidden)

CellInfoMenu = """
<table id='CellInfoMenu' style='width:1000px;'>
    <tr>
        <td>
            <table align='center' style='width:400px;'>
                <tr>
                    <td style='text-align:center;'> 
                        <div id='lblNamedCells'>
                            <b>Named Cells</b>
                        </div>
                        <div id='lnkNamedCells'>
                            <a href='javascript:void();' onClick='javascript:toggleCellInfo("NamedCells");'> 
                                Named Cells
                            </a>
                        </div>
                    </td>
                    <td style='text-align:center;'>
                        <div id='lblPrivateCells'>
                            <b>Private Cells</b>
                        </div>
                        <div id='lnkPrivateCells'> 
                            <a href='javascript:void();' onClick='javascript:toggleCellInfo("PrivateCells");'> 
                                Private Cells 
                            </a> 
                        </div>
                    </td>
                    <td style='text-align:center;'> 
                        <div id='lblSystemCells'>
                            <b>System Cells</b>
                        </div>
                        <div id='lnkSystemCells'> 
                            <a href='javascript:void();' onClick='javascript:toggleCellInfo("SystemCells");'> 
                                System Cells 
                            </a> 
                        <div>
                    </td>
                    <td style='text-align:center;'> 
                        <div id='lblRulesView'>
                            <b>Rules View</b>
                        </div>
                        <div id='lnkRulesView'> 
                            <a href='javascript:void();' onClick='javascript:toggleCellInfo("RulesView");'> 
                                Rules View 
                            </a> 
                        <div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
"""

CellInfo = """
    <table id='tbl%s' style='width:1000px;'>
        <tr>
            <th>Name</th>
            <th>CellMetadataId</th>
            <th>Format</th>
            <th>CellAddress</th>
            <th>ReferenceCell</th>
            <th>FormulaCell</th>
        </tr>
        %s
    </table>
""" # %s (tableid, tablecontent)

rulecontent = """
    <table id='tblRulesView' style="width:100%%;">
        <tr>
            <td>
                <div id='CellNameFormulaBar'>
                    <div id='CellName' class='column' >&nbsp;</div>
                    <div id='label' class='header column'><i>f</i>(x)</div><div id='formulaBar' class='column'>&nbsp;</div>
                </div>
                <table style='border:1px solid;' width=100%%>
                %s
                </table>
            </td>
        </tr>
    </table>
""" # % (rulescontent)

def renderHeader(appName, appDescription):
    return "<h2 style='text-align:center;'> %s </h2><h3 style='text-align:center;'> %s </h3>" % (appName, appDescription)

def renderModelList(SheetMetadataDict):
    modellist = ""
    for key, value in sorted(SheetMetadataDict.iteritems(), key=lambda (k,v): (v,k)):
        modellist = modellist + "\t<option value='" + key + ".htm'>" + value + "</option>\n"
    return modellist

def renderModelHeader(modelName, modelId, showInWorktop, isPublic, isHidden):
    return ModelHeader % (modelName, modelId, showInWorktop, isPublic, isHidden)

def renderCellList(cellmetadatadict):
    PublicCells = ""
    PrivateCells = ""
    SystemCells = ""
    
    for key in cellmetadatadict:
        cellmetadata = cellmetadatadict[key]
        SheetId = cellmetadata["SheetId"]
        CellDisplayName = cellmetadata["CellDisplayName"]
        FinalFormat = cellmetadata["FinalFormat"]
        try:
            CellName = cellmetadata["CellName"]
        except:
            CellName = ""
        try:
            if cellmetadata["ReferredSheetMetadata"]:
                ReferenceCell = 'Yes'
        except:
            ReferenceCell = 'No'
        try:
            if cellmetadata["isFormulaCell"]:
                FormulaCell = 'Yes'
        except:
            FormulaCell = 'No'
            
        if cellmetadata["RowNumber"] > 0 : # User Defined Cells
            if cellmetadata.get("SheetMetadata") is not None: #PublicCells
                PublicCells += NameCellTemplate % (CellName, SheetId, FinalFormat, CellDisplayName, ReferenceCell, FormulaCell)
            else :
                PrivateCells += NameCellTemplate % (CellName, SheetId, FinalFormat, CellDisplayName, ReferenceCell, FormulaCell)
        else : #Platform Defined Cells
            SystemCells += NameCellTemplate % (CellName, SheetId, FinalFormat, CellDisplayName, ReferenceCell, FormulaCell)
    
    tblPublicCells  = CellInfo % ('NamedCells', PublicCells) 
    tblPrivateCells = CellInfo % ('PrivateCells', PrivateCells) 
    tblSystemCells  = CellInfo % ('SystemCells', SystemCells) 
    
    return "<div style='float:left; width:100%;overflow-y:auto;'>" + CellInfoMenu + tblPublicCells + tblPrivateCells + tblSystemCells + "</div>"

def renderRulesView(sheetmetadata):
    rowdata = sheetmetadata.getRows()
    rowName = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ']
    rules = ""
    for key in sorted(rowdata.iterkeys()):
        if key > 0:
            celldata = sheetmetadata.getCells(rowdata[key])
            if key == 1:
                rules += "<tr id='rowHeader'><td class='header'>&nbsp;</td>\n"
                count=0
                while count < len(celldata):
                    rules += "<td class='td header'>%s</td>\n" % (rowName[count])
                    count = count+1
                rules += "</tr>\n"
            rules += "<tr id='%s'><td class='td header'>%s</td>\n" % (rowdata[key],sheetmetadata.SheetMetadata[rowdata[key]]['RowNumber'])
            for key in sorted(celldata.iterkeys()):
                try:
                    if sheetmetadata.SheetMetadata[celldata[key]]["ReferredSheetMetadata"]:
                        style = 'reference'
                except:
                    style = 'public'
                try:
                    rules += "<td id = '%s' class='td %s'><strong>%s</strong></td>\n" % (celldata[key], style, sheetmetadata.SheetMetadata[celldata[key]]["CellName"])
                except:
                    #rules.write("<td id= '%s'>%s</td>" % (celldata[key],key))
                    rules += "<td id='%s' class='td' >&nbsp;</td>\n" % (celldata[key])
            rules += "</tr>\n"
    return rulecontent % (rules)